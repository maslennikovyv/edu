<?php
namespace Application\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 12.08.16
 * Time: 1:07
 */
class Qualification extends Model
{
    protected $primaryKey = 'qualification_id';
    protected $table = 'qualification';
    protected $guarded = [];

    public function user()
    {
        return $this->hasMany(User::class);
    }
}