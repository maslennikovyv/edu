<?php
namespace Application\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 12.08.16
 * Time: 1:07
 */
class User extends Model
{
    protected $primaryKey = 'user_id';
    protected $table = 'user';
    protected $guarded = [];

    public function cities()
    {
        return $this->belongsToMany(City::class);
    }

    public function qualification()
    {
        return $this->belongsTo(Qualification::class);
    }
}