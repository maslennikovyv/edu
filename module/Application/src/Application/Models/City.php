<?php
namespace Application\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 12.08.16
 * Time: 1:07
 */
class City extends Model
{
    protected $primaryKey = 'city_id';
    protected $table = 'city';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsToMany(User::class);
    }


}