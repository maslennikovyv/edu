<?php

namespace Application\Controller;

use Application\Rest\ApiException;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{

    public function indexAction()
    {
        return new ViewModel();
    }

    public function apiAction()
    {
        $id = $this->params()->fromRoute('id');
        $params = $this->params()->fromQuery();

        $request = $this->getRequest();

        $resource = $this->getResource();

        if ($request->isGet()) {

            if ($id) {
                $result = $resource->fetch($id, $params);
            } else {
                $result = $resource->fetchAll($params);
            }

        } elseif ($request->isPost()) {

            $result = $resource->create($this->getParsedBody($request));

        } elseif ($request->isPut()) {

            $result = $resource->update($id, $this->getParsedBody($request));

        } else {

            throw new ApiException(405, 'Method ' . $request->getMetadata('method') . ' has not been defined');
        }

        $response = $this->getResponse();
        $response->setStatusCode(200);
        $response->getHeaders()->addHeaderLine('content-type', 'application/json');
        $response->setContent(Json::encode($result));
        return $response;
    }

    protected function getResource()
    {
        $api = mb_strtolower($this->params()->fromRoute('api'));
        $version = mb_strtolower($this->params()->fromRoute('version'));
        $entity = mb_strtolower($this->params()->fromRoute('entity'));

        $factory_class = '\\Application\\' . ucfirst($api) . '\\' . ucfirst($version) . '\\' . ucfirst($entity) . '\\' . ucfirst($entity) . 'ResourceFactory';

        if (!class_exists($factory_class)) {
            throw new ApiException(404, 'API Resource not found');
        }

        $factory = new $factory_class;

        return $factory($this->getServiceLocator());
    }

    protected function getParsedBody($request)
    {
        $data = json_decode($request->getContent(), true);
        return is_array($data) ? $data : array();
    }

}