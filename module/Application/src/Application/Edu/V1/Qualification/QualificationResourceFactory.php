<?php
namespace Application\Edu\V1\Qualification;


/**
 * Created by PhpStorm.
 * User: user
 * Date: 12.08.16
 * Time: 0:55
 */
class QualificationResourceFactory
{
    public function __invoke($sm)
    {
        return new QualificationResource($sm->get('db'));
    }
}