<?php
namespace Application\Edu\V1\Qualification;

use Application\Models\Qualification;
use Application\Rest\Resource;
use Illuminate\Validation\Validator;
use Symfony\Component\Translation\Translator;


/**
 * Created by PhpStorm.
 * User: user
 * Date: 12.08.16
 * Time: 0:55
 */
class QualificationResource extends Resource
{

    public function fetch($id, $params = array())
    {
        return Qualification::findOrFail($id);
    }

    public function fetchAll($params = array())
    {
        $model = new Qualification();
        return $model->paginate();
    }

    public function create($data)
    {
        $rules = [
            'name' => 'required|min:3',
        ];
        $v = new Validator(new Translator('en'), $data, $rules);

        if ($v->fails()) {
            throw new ApiException(422, 'Validation failure', [
                'validation_errors' => $v->errors()->all(),
            ]);
        }

        $qualification = Qualification::create(['name' => $data['name']]);

        return $qualification->fresh();
    }


}