<?php
namespace Application\Edu\V1\User;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 12.08.16
 * Time: 0:55
 */
class UserResourceFactory
{
    public function __invoke($sm)
    {
        return new UserResource($sm->get('db'));
    }
}