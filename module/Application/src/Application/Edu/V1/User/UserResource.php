<?php
namespace Application\Edu\V1\User;

use Application\Models\Qualification;
use Application\Models\User;
use Application\Rest\ApiException;
use Application\Rest\Resource;
use Illuminate\Validation\Validator;
use Symfony\Component\Translation\Translator;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 12.08.16
 * Time: 0:55
 */
class UserResource extends Resource
{

    public function fetch($id, $params = array())
    {
        return Qualification::findOrFail($id);
    }

    public function fetchAll($params = array())
    {
        $model = new User;

        if (isset($params['by_qualifications'])) {
            $ids = array_filter(explode(',', $params['by_qualifications']), 'intval');
            $model = $model->whereIn('qualification_id', $ids);
        }

        if (isset($params['by_cities'])) {
            $ids = array_filter(explode(',', $params['by_cities']), 'intval');
            $model = $model->whereExists(function ($query) use ($ids) {
                $query->selectRaw('1')
                    ->from('city_user')
                    ->whereRaw('city_user.user_id = "user".user_id')
                    ->whereIn('city_id', $ids);
            });

        }

        return $model->with(['qualification', 'cities'])->get();
    }

    public function create($data)
    {
        return $this->update(0, $data);
    }

    public function update($id, $data)
    {
        $rules = [
            'name' => 'required|string|between:3,100',
            'qualification' => 'sometimes|string|between:3,100',
            'cities' => 'sometimes|array',
        ];
        $v = new Validator(new Translator('en'), $data, $rules);

        if ($v->fails()) {
            throw new ApiException(422, 'Validation failure', [
                'validation_errors' => $v->errors()->all(),
            ]);
        }

        if ($id) {
            // Update
            $user = User::findOrFail($id);
        } else {
            // Create
            $user = User::create(['name' => $data['name']]);
        }

        if (isset($data['qualification'])) {
            $qualification = Qualification::firstOrCreate([
                'name' => $data['qualification'],
            ]);

            $qualification->user()->save($user);
        }

        if (isset($data['cities'])) {
            $user->cities()->sync($data['cities']);
        }

        return $user->fresh('qualification', 'cities');
    }


}