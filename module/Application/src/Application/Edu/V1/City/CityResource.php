<?php
namespace Application\Edu\V1\City;

use Application\Models\City;
use Application\Rest\Resource;
use Illuminate\Validation\Validator;
use Symfony\Component\Translation\Translator;


/**
 * Created by PhpStorm.
 * User: user
 * Date: 12.08.16
 * Time: 0:55
 */
class CityResource extends Resource
{

    public function fetch($id, $params = array())
    {
        return City::findOrFail($id);
    }

    public function fetchAll($params = array())
    {
        $model = new City();


        $model->orderBy(['title']);

        if (isset($params['search'])) {


        }

        return $model->get();
    }

    public function create($data)
    {
        $rules = [
            'name' => 'required|min:3',
        ];
        $v = new Validator(new Translator('en'), $data, $rules);

        if ($v->fails()) {
            throw new ApiException(422, 'Validation failure', [
                'validation_errors' => $v->errors()->all(),
            ]);
        }

        $city = City::create(['name' => $data['name']]);

        return $city->fresh();

    }


}