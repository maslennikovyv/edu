<?php
namespace Application\Edu\V1\City;


/**
 * Created by PhpStorm.
 * User: user
 * Date: 12.08.16
 * Time: 0:55
 */
class CityResourceFactory
{
    public function __invoke($sm)
    {
        return new CityResource($sm->get('db'));
    }
}