<?php

use Phinx\Migration\AbstractMigration;

class Initial extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $sql = <<<EOT
DROP TABLE IF EXISTS "user";

CREATE TABLE "user"
(
  user_id bigserial NOT NULL,
  name character varying(255),
  qualification_id bigint,
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  CONSTRAINT user_pkey PRIMARY KEY (user_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "user"
  OWNER TO "user";
  
DROP TABLE IF EXISTS "qualification";

CREATE TABLE "qualification"
(
  qualification_id bigserial NOT NULL,
  name character varying(255),
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  CONSTRAINT qualification_pkey PRIMARY KEY (qualification_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "qualification"
  OWNER TO "user";
  
DROP TABLE IF EXISTS "city";

CREATE TABLE "city"
(
  city_id bigserial NOT NULL,
  name character varying(255),
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  CONSTRAINT city_pkey PRIMARY KEY (city_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE "city"
  OWNER TO "user";
  
DROP TABLE city_user;

CREATE TABLE city_user
(
  city_id bigint NOT NULL,
  user_id bigint NOT NULL,
  CONSTRAINT city_user_pkey PRIMARY KEY (city_id, user_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE city_user
  OWNER TO "user";


EOT;

        $this->execute($sql);

    }
}